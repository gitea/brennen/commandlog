=pod

=head1 NAME

App::CommandLog - Tools for logging command line history

=head1 INSTALLING

    $ perl Build.PL
    $ ./Build
    $ ./Build install

=cut

package App::CommandLog;

our ($VERSION) = '0.5.0';

use strict;
use warnings;

use DBI;

=over

=item get_dbh()

Get database handle for default commandlog database.

=cut

sub get_dbh {
  my $dbfile = $ENV{HOME} . "/cli.db";

  my $init_new = 0;
  $init_new = 1 unless -f $dbfile;

  my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile", "", "");

  # Make a new database and table(s), if needed:
  create_log_db($dbh) if $init_new;

  return $dbh;
}

=item create_log_db()

Create SQLite log database.

=cut

sub create_log_db {
  # TODO: maybe log exit status, timing, memory, load, distro
  my ($dbh) = @_;
  $dbh->do(<<'SQL'
    CREATE TABLE commands (
      id integer primary key,
      command text,
      expanded_command text,
      path text,
      hostname text,
      username text,
      shell text,
      terminal text,
      notes text,
      datetime text
    );
SQL
  );
}

=back

=head1 AUTHOR

Copyright 2016-2019 Brennen Bearnes

    commandlog is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    commandlog is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with commandlog.  If not, see <https://www.gnu.org/licenses/>.
    commandlog is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

=cut

1;
