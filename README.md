commandlog
==========

A small set of tools for logging and annotating command-line history in a
Linux or Unixlike environment.  Requires Perl, sqlite, and zsh.  (It
probably won't take much to add to Bash, but I haven't tried yet.)

So far, this is about an hour's worth of messing around one evening.  It
doesn't do much.  Here's what I know works:

```sh
# assuming Debian/Ubuntu and sqlite not already installed:
sudo apt-get install sqlite3

git clone git@github.com:brennen/commandlog
cd commandlog
perl Build.PL
sudo ./Build installdeps
sudo ./Build install
```

Next, put the following in your `.zshrc`:

```zsh
function preexec {
  commandlog add "$@"
}
```

Or, if using Oh-My-Zsh:

```zsh
autoload -U add-zsh-hook
function commandlog_preexec {
  commandlog add "$@"
}
add-zsh-hook preexec commandlog_preexec
```

Now, with any luck, you can use:

```sh
commandlog log
```

And you should get output something like the following:

    2018-03-23 05:29:22	v lib/App/CommandLog.pm
    2018-03-23 05:29:09	vim Build.PL
    2018-03-23 05:29:04	ll
    2018-03-23 05:28:54	fg
    2018-03-23 05:28:52	c log
    2018-03-23 05:28:43	sudo ./Build install
    2018-03-23 05:28:40	./Build install
    2018-03-23 05:28:37	cd ..
    2018-03-23 05:28:34	ll
    2018-03-23 05:28:32	mv commandlog-log.pl commandlog-log

No promises though.  If anybody tries this and has thoughts, please do let me
know.
